<?php

namespace Dse\ElementsBundle\ElementGooglemapteaser\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementGooglemapteaser;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementGooglemapteaser\DseElementGooglemapteaser::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
