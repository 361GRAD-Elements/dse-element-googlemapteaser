<?php

/**
 * 361GRAD Element Googlemapteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_googlemapteaser'] =
    'Dse\\ElementsBundle\\ElementGooglemapteaser\\Element\\ContentDseGooglemapteaser';
