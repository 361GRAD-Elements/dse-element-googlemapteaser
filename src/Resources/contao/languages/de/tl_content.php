<?php

/**
 * 361GRAD Element Googlemapteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = "DSE-Elemente";
$GLOBALS['TL_LANG']['CTE']['dse_googlemapteaser'] = ["Google Map", "Google Map"];

$GLOBALS['TL_LANG']['tl_content']['subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];
$GLOBALS['TL_LANG']['tl_content']['dse_latitude'] =
    ['Breite', 'Bitte geben Sie den Breitengrad ein.'];
$GLOBALS['TL_LANG']['tl_content']['dse_longitude'] =
    ['Länge', 'Bitte geben Sie den Längengrad ein.'];
$GLOBALS['TL_LANG']['tl_content']['dse_maplink'] =
    ['Roadmap link', 'Hier können Sie eine Roadmap-Link hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_mapmarker'] =
    ['Kartenmarkerbild', 'Hier können Sie einen Kartenmarker hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];
